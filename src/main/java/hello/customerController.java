package hello;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController("Контрагент")
@RequestMapping("/customer")
public class customerController {

    private static final String template = "Hello, %s!";


    @RequestMapping(value="/{customer_id}",method = RequestMethod.DELETE,produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Удаление записи контрагента")
    public Customer Customer(@RequestParam(value="name", defaultValue="World") String name) {
        Integer customer_id=null;
        return new Customer(customer_id,
                String.format(template, name));
    }

    @RequestMapping(value="/{customer_id}?name=",method = RequestMethod.PUT,produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Изменение записи контрагента")
    public Customer Customer(@PathVariable(value="ИД контрагента") Integer customer_id,@RequestParam(value="name", defaultValue="World") String name) {
        return new Customer(customer_id,
                String.format(template, name));
    }




    @RequestMapping(value="/{customer_id}",method = RequestMethod.GET,produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody()
    @ApiOperation("Получение данных по определенному контрагенту")
    public Customer Customer(@PathVariable Integer customer_id) {
        return new Customer(customer_id,
                String.format(template, customer_id));
    }


    @RequestMapping(value="",method = RequestMethod.POST ,produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation("Добавления записи контрагента")
    public Customer Customer() {
        Integer customer_id=null;
        return new Customer(customer_id,
                "new customer");
    }





}
