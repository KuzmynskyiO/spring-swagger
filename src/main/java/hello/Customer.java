package hello;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.context.annotation.Description;

public class Customer {
    @ApiModelProperty(notes = "Уникальный идентификатор контрагента")
    private final Integer id;

    @ApiModelProperty(notes = "Краткое наименование клиента")
    private final String shortName;

    public Customer(Integer id, String shortName) {
        this.id = id;
        this.shortName = shortName;
    }


    public String getShortName() {
        return shortName;
    }

    public long getId() {
        return id;
    }
}
